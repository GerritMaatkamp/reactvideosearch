import axios from 'axios'

const KEY = 'AIzaSyDvNPC7Wn71C3rT6w-XL2QtmGIVeCuS9es';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: KEY
    }
})