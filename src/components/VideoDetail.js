import React from 'react'
import "./VideoDetail.css"

const VideoDetail = ({ video }) => {
    if(!video) {
        return <div>Loading...</div>
    }

    const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`

    return (
        <div>
            <div className="ui segment">
                <iframe src={ videoSrc } title="video player" />
                <h4 className="ui header">
                    { video.snippet.title }
                </h4>
                <p className="ui content">
                    { video.snippet.description }
                </p>
            </div>
        </div>
    )
}

export default VideoDetail;